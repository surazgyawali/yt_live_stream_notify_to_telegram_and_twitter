import os
import pickle
from datetime import datetime, timezone

from google_auth_oauthlib import flow
import googleapiclient.discovery
import googleapiclient.errors
from google.auth.transport.requests import Request

import config

credentials = None

if os.path.exists("token.pickle"):
    print("Loading Credentials From File...")
    with open("token.pickle", "rb") as token:
        credentials = pickle.load(token)

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


if not credentials or not credentials.valid:
    if credentials and credentials.expired and credentials.refresh_token:
        print("Refreshing Access Token...")
        credentials.refresh(Request())
    else:
        print("Fetching New Tokens...")
        flow = flow.InstalledAppFlow.from_client_secrets_file(
            config.YT_CLIENT_SECERET_FILE2,
            scopes=["https://www.googleapis.com/auth/youtube.readonly"],
        )

        flow.run_local_server(
            port=config.PORT, prompt="consent", authorization_prompt_message=""
        )
        credentials = flow.credentials

        # Save the credentials for the next run
        with open("token.pickle", "wb") as f:
            print("Saving Credentials for Future Use...")
            pickle.dump(credentials, f)

def get_yt_stream(credintials=credentials):
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"

    youtube = googleapiclient.discovery.build(
            api_service_name, api_version, credentials=credentials)

    request = youtube.liveBroadcasts().list(
        part="snippet,id,contentDetails",
        broadcastStatus="active",
    )

    response = request.execute()
    if response["items"]:
        data = response["items"][0]
        stream_id = data["id"]
        stream_start = data["snippet"]["actualStartTime"]
        diff = datetime.now(timezone.utc) - datetime.strptime(stream_start, "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
        diffseconds = diff.seconds
        watch_url = f"https://youtu.be/{stream_id}?t={diffseconds}s"
        return watch_url
    else:
        return False