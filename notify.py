from logging import exception
import os
import requests
from datetime import datetime, timezone

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors

import tweepy
import telegram
import config
import yt


def send_to_twitter(tweet):
    auth = tweepy.OAuthHandler(
        config.TWITTER_CONSUMER_KEY, config.TWITTER_CONSUMER_SECRET
    )
    auth.set_access_token(
        config.TWITTER_ACCESS_TOKEN, config.TWITTER_ACCESS_TOKEN_SECRET
    )
    api = tweepy.API(auth)
    status = api.update_status(status=tweet)


def send_to_telegram(message):
    bot = telegram.Bot(token=config.TELEGRAM_BOT_TOKEN)
    bot.send_message(chat_id=config.TELEGRAM_CHAT_ID, text=message)

if __name__ == "__main__":
    # Get the current time
    currtime = datetime.now(timezone.utc).strftime("%H:%M:%S")
    live_streams = yt.get_yt_stream()
    if live_streams:
        message = config.MESSAGE.format(currtime, live_streams)
        print(f'\n{message}\n')
        try:
            send_to_twitter(message)
            pass
        except Exception as e:
            print(f'Something went wrong while posting to twitter reason:\n{e}\n')


        try:
            send_to_telegram(message)
        except Exception as e:
            print(f'Something went wrong while posting to telegram reason:\n{e}\n')

    else:
        print(f"No live streams detected at the moment.")
        send_to_telegram(f"No live streams detected at the moment.")
        input("Press Enter to exit!!!")
        exit()
