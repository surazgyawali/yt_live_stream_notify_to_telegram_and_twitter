# ------------------------------------------ Project Configuration -------------------------------------------------

PORT = 8080
STREAM_DELAY = 0

MESSAGE = "ALERT! BEEP TONE DETECTED AT {0} Tune in now to listen live. {1}"

# 0 will be time and 1 will be the link can be arranged in any manner.
# Example:
# MESSAGE = ALERT AT {0} Click {1} now to catch the action.

# ------------------------------------------------ API CONFIGURATION ------------------------------------------------
# for twitter

TWITTER_CONSUMER_KEY = "REPLACE_THIS_WITH_YOUR_CONSUMER_KEY"
TWITTER_CONSUMER_SECRET = "REPLACE_THIS_WITH_YOUR_CONSUMER_SECRET"
TWITTER_ACCESS_TOKEN = "REPLACE_THIS_WITH_YOUR_ACCESS_TOKEN"
TWITTER_ACCESS_TOKEN_SECRET = "REPLACE_THIS_WITH_YOUR_ACCESS_TOKEN_SECRET"

# for telegram
TELEGRAM_BOT_TOKEN = ""
TELEGRAM_CHAT_ID = ""

#for youtube

# include full path to the credentials file
YT_CLIENT_SECERET_FILE = "./google_oauth_localhost.json"
